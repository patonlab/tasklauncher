# -*- coding: utf-8 -*-
"""
Created on Fri Aug 10 16:15:37 2012
Modified on Sat Dec 27 15:53:00 2014

@author: thiago
"""

import serial
import datetime
import timeit, time
import os
import Tkinter as tk
import tkFileDialog as tkfd

PADX = 10;

class tasklauncher(object):    
    def __init__(self):
        self.root = tk.Tk()
        self.root.title("Task Launcher")
        self.root.geometry("800x600")

        self.fmode = 'wb'
        
        self.filename = tk.StringVar(self.root)
        self.fullpath = tk.StringVar(self.root)
        self.pdir = tk.StringVar(self.root)
        self.fdir = tk.StringVar(self.root)
        self.animalName = tk.StringVar(self.root)
        self.experimenterName = tk.StringVar(self.root)
        self.animalWeight = tk.StringVar(self.root)
        self.boxName = tk.StringVar(self.root)
        self.baudRate = tk.StringVar(self.root)
        self.taskName = tk.StringVar(self.root)
        self.date = tk.StringVar(self.root)
        self.dataRaw = ""
        
        tk.Label(self.root,text="TaskLauncher",font="Liberation 14 bold").grid(columnspan=5,padx=PADX,pady=50)
        
        tk.Label(self.root,text="Project directory").grid(row=1,column=0,sticky=tk.E,padx=PADX)
        self.pdir.set(os.path.normpath('C:\\Users\\t_gou_000\\Dropbox (Learning Lab)\\PatonLab\\Data\\AlloDiscr'))
        self.lb_pdir = tk.Label(self.root,textvariable=self.pdir,wraplength=500,justify=tk.LEFT).grid(row=1,column=1,columnspan=3,sticky=tk.W,padx=PADX)
        self.bt_pdir = tk.Button(self.root,text='...',command=self.askpdir).grid(row=1,column=4,padx=PADX)
        
        tk.Label(self.root,text="File directory").grid(row=2,column=0,sticky=tk.E,padx=PADX)
        self.lb_fdir = tk.Label(self.root,textvariable=self.fdir,wraplength=500,justify=tk.LEFT).grid(row=2,column=1,columnspan=3,sticky=tk.W,padx=PADX)
        
        tk.Label(self.root,text="Animal ID").grid(row=4,column=0,sticky=tk.E,padx=PADX)
        self.animalName.set('Zero')
        self.animalList = tk.OptionMenu(self.root,self.animalName,'Zero','Ariel','Branko','Cazuza','Dominguinhos','Elza','not listed',command=self.updatePath)
        self.animalList.config(width=20)
        self.animalList.grid(row=4,column=1,sticky=tk.W,padx=PADX)

        tk.Label(self.root,text="Experimenter ID").grid(row=3,column=0,sticky=tk.E,padx=PADX)
        self.experimenterName.set('TG')
        self.experimenterList = tk.OptionMenu(self.root,self.experimenterName,'TG','TM','AB','not listed',command=self.updatePath)
        self.experimenterList.config(width=20)
        self.experimenterList.grid(row=3,column=1,sticky=tk.W,padx=PADX)
        
        tk.Label(self.root,text="Animal weight").grid(row=5,column=0,sticky=tk.E,padx=PADX)
        self.animalWeightBox = tk.Entry(self.root,textvar=self.animalWeight,width=10).grid(row=5,column=1,sticky=tk.W,padx=PADX)
        
        tk.Label(self.root,text="Serial port").grid(row=7,column=0,sticky=tk.E,padx=PADX)
        self.boxName.set('COM5')
        self.boxList = tk.OptionMenu(self.root,self.boxName,'COM5','COM8','not listed')
        self.boxList.config(width=20)
        self.boxList.grid(row=7,column=1,sticky=tk.W,padx=PADX)

        tk.Label(self.root,text="Baud rate").grid(row=7,column=2,sticky=tk.E,padx=PADX)
        self.baudRate.set(115200)
        self.baudRateList = tk.Entry(self.root,textvariable=self.baudRate).grid(row=7,column=3,sticky=tk.W,padx=PADX)
        
        tk.Label(self.root,text="Task").grid(row=6,column=0,sticky=tk.E,padx=PADX)
        self.taskName.set('ALLODISCRv01')
        self.taskList = tk.OptionMenu(self.root,self.taskName,'FR1v01','MATCH4PKv01','ALLODISCRv02','not listed',command=self.updatePath)
        self.taskList.config(width=20)
        self.taskList.grid(row=6,column=1,sticky=tk.W,padx=PADX)

        self.date.set(datetime.date.today().strftime("%y%m%d"))
        
        tk.Label(self.root,text="Full path").grid(row=8,column=0,sticky=tk.E,padx=PADX)
        self.lb_fullpath = tk.Entry(self.root,textvariable=self.fullpath,width=100,justify=tk.LEFT).grid(row=8,column=1,columnspan=3,sticky=tk.W,padx=PADX)

        self.btLaunch = tk.Button(self.root,text='Launch',fg='forest green',command=self.launch,font='Liberation 12 bold').grid(row=10,column=1,padx=PADX)
        self.btPause = tk.Button(self.root,text='Pause',fg='darkgoldenrod1',command=self.pause,font='Liberation 12 bold').grid(row=10,column=2,padx=PADX)
        self.btKill = tk.Button(self.root,text='Kill',fg='firebrick1',command=self.kill,font='Liberation 12 bold').grid(row=10,column=3,padx=PADX)

        self.Matrix = tk.Label()
        
        self.updatePath(self.animalName.get())

        self.root.mainloop()
        
    def askpdir(self):
        self.pdir.set(tkfd.askdirectory(parent=self.root,title='Pick your pdir'))
        self.setfdir(self.animalName.get())
        
    def updatePath(self,garbage):
        self.fdir.set(os.path.join(self.pdir.get(),'Behavior',self.animalName.get()))
        self.filename.set(self.animalName.get() + '_' + self.taskName.get() + '_' + self.date.get()  + '_' + self.experimenterName.get() + '.txt')
        self.fullpath.set(os.path.join(self.fdir.get(),self.filename.get()))

    def launch(self):
        self.checkSerial()
        self.checkDir(self.fdir.get())
        self.checkFilePath(self.fullpath.get())
        self.output = open(self.fullpath.get(),self.fmode)
        if self.fmode == 'wb':
            self.printHeader()
        self.serial.write('s')
        print('Printed s to the serial port')
        self.timer = timeit.default_timer()
        self.root.after(100,self.readSerial())

    def pause(self):
        self.serial.close()
        if not self.serial.isOpen():
            print('Serial port closed.\r\nClicking Launch again might cause a bug (to be tested and fixed)')

    def kill(self):
        self.serial.close()
        self.root.destroy()

    def readSerial(self):
        if self.serial.isOpen():
            if self.serial.inWaiting()>0:
                try:
                    temp = self.serial.readline()
                    print(temp)
                    self.output.write(temp)
                    del temp
                    self.output.flush()
                except:
                    print "Error"
                    self.output.write("Error")
                    self.output.flush() 
            if timeit.default_timer()-self.timer > 60:
                self.timer = timeit.default_timer()
                self.output.close()
                self.output = open(self.fullpath.get(),'ab')
            self.root.after(100,self.readSerial)

    def checkDir(self,fdir):
        if not os.path.isdir(fdir):
            self.dialDir = tk.Toplevel(self.root)
            self.dialDir.geometry("400x300")
            tk.Message(self.dialDir,text='No directory named \n\n' + fdir + '\n\nCreate directory?',width=300).grid(columnspan=3,padx=PADX,pady=5*PADX)
            tk.Button(self.dialDir,text='Yes',command=lambda: self.makeDir(fdir)).grid(row=1)
            tk.Button(self.dialDir,text='Cancel',command=lambda: self.dialDir.destroy()).grid(row=1,column=1)
            self.root.wait_window(self.dialDir)

    def checkFilePath(self,fullpath):
        if os.path.isfile(fullpath):
            self.dialFile = tk.Toplevel(self.root)
            self.dialFile.geometry("400x300")
            tk.Message(self.dialFile,text='A file named ' + self.filename.get() + '\nalready exists.\nOverwrite or append?',width=300,justify=tk.CENTER).grid(columnspan=3,padx=PADX,pady=5*PADX)
            tk.Button(self.dialFile,text='Overwrite',command=self.fileOverwrite).grid(row=1)
            tk.Button(self.dialFile,text='Append',command=self.fileAppend).grid(row=1,column=1)
            self.root.wait_window(self.dialFile)

    def checkSerial(self):
        self.serial = serial.Serial(self.boxName.get(),self.baudRate.get())
        if not self.serial.isOpen():
            print('Opening serial')
            self.serial.open()
        else:
            print('Serial was already open')
        time.sleep(1)

    def fileOverwrite(self):
        self.fmode = 'wb'
        self.dialFile.destroy()

    def fileAppend(self):
        self.fmode = 'ab'
        self.dialFile.destroy()
    
    def makeDir(self,fdir):
        os.mkdir(fdir)
        self.dialDir.destroy()

    def printHeader(self):
        experimenterAscii = ""
        for char in self.experimenterName.get():
            experimenterAscii = experimenterAscii + '125\t' + str(ord(char)) + '\r\n'

        animalAscii = ""
        for char in self.animalName.get():
            animalAscii = animalAscii + '103\t' + str(ord(char)) + '\r\n'

        weightAscii = ""
        for char in self.animalWeight.get():
            weightAscii = weightAscii + '115\t' + str(ord(char)) + '\r\n'

        taskAscii = ""
        for char in self.taskName.get():
            taskAscii = taskAscii + '126\t' + str(ord(char)) + '\r\n'

        dateAscii = ""
        for char in self.date.get():
            dateAscii = dateAscii + '127\t' + str(ord(char)) + '\r\n'

        temp = animalAscii + experimenterAscii + taskAscii + dateAscii + weightAscii
        print(temp)
        self.dataRaw += temp
        self.output.write(temp)
        del temp
        
        self.output.close()
        self.fmode = 'ab'
        self.output = open(self.fullpath.get(),self.fmode)

class tasklauncher_plots(tasklauncher):
    def __init__(self):
        print("You'll have to wait for that")

app1 = tasklauncher()
